package echo;
import echo.util.JavalinApp;

public class Main {
    public static void main(String[] args) {
        JavalinApp app = new JavalinApp();
        app.start(80);
    }
}
